import { DateService } from './../../services/date.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-month-btn',
  templateUrl: './month-btn.component.html',
  styleUrls: ['./month-btn.component.scss']
})
export class MonthBtnComponent implements OnInit {
  private date: Date;

  constructor(private dateService: DateService) { }

  ngOnInit(): void {
    this.date = this.dateService.getDate();
  }

  public getDate(): Date {
    return this.date;
  }

}
