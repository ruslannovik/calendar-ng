import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthBtnComponent } from './month-btn.component';

describe('MonthBtnComponent', () => {
  let component: MonthBtnComponent;
  let fixture: ComponentFixture<MonthBtnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonthBtnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
