import { DateService } from './../../services/date.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-years-btn',
  templateUrl: './years-btn.component.html',
  styleUrls: ['./years-btn.component.scss']
})
export class YearsBtnComponent implements OnInit {
  private dates: string;

  constructor(private dateService: DateService) { }

  ngOnInit(): void {
    this.createDates();
  }

  private createDates(): void {
    const firstYear = this.dateService.getDate().getFullYear();
    const lastYear = firstYear + 23;
    this.dates = `${firstYear}-${lastYear}`;
  }

  public getDates(): string {
    return this.dates;
  }

}
