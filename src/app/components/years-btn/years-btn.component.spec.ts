import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YearsBtnComponent } from './years-btn.component';

describe('YearsBtnComponent', () => {
  let component: YearsBtnComponent;
  let fixture: ComponentFixture<YearsBtnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ YearsBtnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YearsBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
