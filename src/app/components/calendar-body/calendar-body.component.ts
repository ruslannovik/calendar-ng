import { MonthViewComponent } from './../month-view/month-view.component';
import { AdDirective } from './../../directives/ad.directive';
import { Component, Input, OnInit, ViewChild, ComponentFactoryResolver, ComponentFactory, OnChanges, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'app-calendar-body',
  templateUrl: './calendar-body.component.html',
  styleUrls: ['./calendar-body.component.scss']
})
export class CalendarBodyComponent implements OnChanges {
  @ViewChild(AdDirective, {static: true}) adDirective: AdDirective;
  @Input() calendarState: string;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnChanges(): void {
    this.changeView();
  }

  private changeView(): void {
    const viewContainerRef = this.adDirective.viewContainerRef;
    viewContainerRef.clear();

    if (this.calendarState === 'month') {
        const monthViewFactory = this.componentFactoryResolver.resolveComponentFactory(MonthViewComponent);
        viewContainerRef.createComponent(monthViewFactory);
    }
  }
}
