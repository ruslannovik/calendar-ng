import { CalendarService } from './../../services/calendar.service';
import { YearsBtnComponent } from './../years-btn/years-btn.component';
import { YearBtnComponent } from './../year-btn/year-btn.component';
import { MonthBtnComponent } from './../month-btn/month-btn.component';
import { AdDirective } from './../../directives/ad.directive';
import { Component, ComponentFactory, ComponentFactoryResolver, Input, OnChanges, ViewChild} from '@angular/core';

@Component({
  selector: 'app-calendar-header',
  templateUrl: './calendar-header.component.html',
  styleUrls: ['./calendar-header.component.scss']
})
export class CalendarHeaderComponent implements OnChanges {
  @ViewChild(AdDirective, {static: true}) controlBtn: AdDirective;
  @Input() calendarState: string;

  private monthBtnFactory: ComponentFactory<MonthBtnComponent>;
  private yearBtnFactory: ComponentFactory<YearBtnComponent>;
  private yearsBtnFactory: ComponentFactory<YearsBtnComponent>;

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private calendarService: CalendarService) { }

  ngOnChanges(): void {
    this.changeBtn();
  }

  changeBtn() {
    console.log(this.controlBtn)
    const viewContainerRef = this.controlBtn.viewContainerRef;
    viewContainerRef.clear();

    if (this.calendarState === 'month') {
      this.monthBtnFactory = this.componentFactoryResolver.resolveComponentFactory(MonthBtnComponent);
      viewContainerRef.createComponent(this.monthBtnFactory);
    } else if (this.calendarState === 'year') {
      this.yearBtnFactory = this.componentFactoryResolver.resolveComponentFactory(YearBtnComponent);
      viewContainerRef.createComponent(this.yearBtnFactory);
    } else if (this.calendarState === 'years') {
      this.yearsBtnFactory = this.componentFactoryResolver.resolveComponentFactory(YearsBtnComponent);
      viewContainerRef.createComponent(this.yearsBtnFactory);
    }
  }

  public changeState(): void {
    if (this.calendarState === 'month') {
      this.calendarService.chooseYears();
    } else {
      this.calendarService.chooseMonth();
    }
  }

}
