import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YearBtnComponent } from './year-btn.component';

describe('YearBtnComponent', () => {
  let component: YearBtnComponent;
  let fixture: ComponentFixture<YearBtnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ YearBtnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YearBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
