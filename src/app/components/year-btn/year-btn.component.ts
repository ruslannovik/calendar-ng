import { DateService } from './../../services/date.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-year-btn',
  templateUrl: './year-btn.component.html',
  styleUrls: ['./year-btn.component.scss']
})
export class YearBtnComponent implements OnInit {
  private date: Date;

  constructor(private dateService: DateService) { }

  ngOnInit(): void {
    this.date = this.dateService.getDate();
  }

  public getDate(): Date {
    return this.date;
  }

}
