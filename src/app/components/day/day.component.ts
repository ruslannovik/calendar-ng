import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-day',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.scss']
})
export class DayComponent implements OnInit {
  private _day: number;

  constructor() { }

  ngOnInit(): void {
  }

  @Input() set day(day: number) {
    this._day = day;
  }

  getDay() {
    return this._day;
  }

}
