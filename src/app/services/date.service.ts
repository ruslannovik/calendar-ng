import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateService {
  private chosenDate: Date;
  private currentDate: Date;

  constructor() {
    this.currentDate = new Date();
    this.chosenDate = this.currentDate;
  }

  getDate(): Date {
    return this.chosenDate;
  }

  setDate(date: Date) {
    this.chosenDate = date;
  }
}
