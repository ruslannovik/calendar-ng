import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {
  private calendarState: string;

  constructor() {
    this.calendarState = 'month';
  }

  public chooseMonth(): void {
    this.calendarState = 'month';
  }

  public chooseYear(): void {
    this.calendarState = 'year';
  }

  public chooseYears(): void {
    this.calendarState = 'years';
  }

  public getCalendarState(): string {
    return this.calendarState;
  }

}
