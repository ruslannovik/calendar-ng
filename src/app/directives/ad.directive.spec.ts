import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CalendarHeaderComponent } from '../components/calendar-header/calendar-header.component';
import { AdDirective } from './ad.directive';

describe('AdDirective', () => {
  let fixture: ComponentFixture<CalendarHeaderComponent>;

  beforeEach(() => {
    fixture = TestBed.configureTestingModule({
      declarations: [CalendarHeaderComponent]
    })
    .createComponent(CalendarHeaderComponent);
    fixture.detectChanges();
  })

  it('should create an instance', () => {
    let elRefMock = {
      nativeElement: document.createElement('div')
    };

    const directive = new AdDirective(fixture.nativeElement.querySelector('ng-template'));
    expect(directive).toBeTruthy();
  });
});
