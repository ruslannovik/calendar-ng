import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DayComponent } from './components/day/day.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { CalendarHeaderComponent } from './components/calendar-header/calendar-header.component';
import { AdDirective } from './directives/ad.directive';
import { MonthBtnComponent } from './components/month-btn/month-btn.component';
import { YearBtnComponent } from './components/year-btn/year-btn.component';
import { YearsBtnComponent } from './components/years-btn/years-btn.component';
import { CalendarBodyComponent } from './components/calendar-body/calendar-body.component';
import { MonthViewComponent } from './components/month-view/month-view.component';

@NgModule({
  declarations: [
    AppComponent,
    DayComponent,
    CalendarComponent,
    CalendarHeaderComponent,
    AdDirective,
    MonthBtnComponent,
    YearBtnComponent,
    YearsBtnComponent,
    CalendarBodyComponent,
    MonthViewComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
